package it.uniroma2.art.semanticturkey.services.core.genoma;

public class GENOMAException extends Exception {

	private static final long serialVersionUID = 1L;

	public GENOMAException(String message) {
		super(message);
	}
}
