package it.uniroma2.art.semanticturkey.extension.impl.repositoryimplconfigurer.predefined;

public class GraphDBSEConfigurerConfiguration extends AbstractGraphDBConfigurerConfiguration {

	@Override
	public String getShortName() {
		return "GraphDB SE (remote only)";
	}

}
