package it.uniroma2.art.semanticturkey.extension.impl.repositoryimplconfigurer.predefined;

import it.uniroma2.art.semanticturkey.config.Configuration;

public interface PredefinedRepositoryImplConfigurerConfiguration extends Configuration {

}
