package it.uniroma2.art.semanticturkey.extension.impl.repositoryimplconfigurer.predefined;

public class GraphDBFreeConfigurerConfiguration extends AbstractGraphDBConfigurerConfiguration {

	@Override
	public String getShortName() {
		return "GraphDB Free (remote only)";
	}

}
